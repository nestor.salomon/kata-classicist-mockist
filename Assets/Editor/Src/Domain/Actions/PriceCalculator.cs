using Editor.Src.Domain.Model;
using Editor.Src.Domain.Model.Interfaces;

namespace Editor.Src.Domain.Actions
{
    public class PriceCalculator : IPriceCalculator
    {
        public float CalculateProductPrice(Product product, int quantity)
        {
            return product.Promotion == null
                ? product.Price * quantity
                : product.Promotion.CalculatePrice(product.Price, quantity);
        }
    }
}