namespace Editor.Src.Domain.Model.Interfaces
{
    public interface IPromotion
    {
        string Name { get; }

        float CalculatePrice(float unitPrice, int quantity);
    }
}