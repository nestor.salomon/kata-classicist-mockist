using System.Collections.Generic;

namespace Editor.Src.Domain.Model.Interfaces
{
    public interface IProductRepository
    {
        IEnumerable<Product> ListProductsByIds(IEnumerable<int> ids);
    }
}