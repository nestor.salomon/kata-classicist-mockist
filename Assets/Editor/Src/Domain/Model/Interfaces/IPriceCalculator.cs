namespace Editor.Src.Domain.Model.Interfaces
{
    public interface IPriceCalculator
    {
        float CalculateProductPrice(Product product, int quantity);
    }
}