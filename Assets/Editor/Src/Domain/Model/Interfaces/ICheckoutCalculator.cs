using System.Collections.Generic;

namespace Editor.Src.Domain.Model.Interfaces
{
    public interface ICheckoutCalculator
    {
        Checkout CalculateCheckout(IEnumerable<SelectedProduct> selection, IEnumerable<Product> products);
    }
}