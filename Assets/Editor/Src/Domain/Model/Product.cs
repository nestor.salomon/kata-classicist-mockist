﻿using Editor.Src.Domain.Model.Interfaces;

namespace Editor.Src.Domain.Model
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public float Price { get; set; }
        public IPromotion Promotion { get; set; }
    }
}
