namespace Editor.Src.Domain.Model
{
    public class Checkout
    {
        public CheckoutItem[] Items { get; set; }
        public float Total { get; set; }
    }
}