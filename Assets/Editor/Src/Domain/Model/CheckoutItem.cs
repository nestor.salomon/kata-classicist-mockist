namespace Editor.Src.Domain.Model
{
    public class CheckoutItem
    {
        public Product Product { get; set; }
        public float Price { get; set; }
    }
}