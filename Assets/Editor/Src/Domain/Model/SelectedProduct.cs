namespace Editor.Src.Domain.Model
{
    public class SelectedProduct
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
    }
}